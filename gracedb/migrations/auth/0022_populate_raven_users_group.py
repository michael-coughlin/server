# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-05-08 16:22
from __future__ import unicode_literals

from django.db import migrations

GROUP_NAME = 'raven_users'
USERS = [
    'brandon.piotrzkowski@LIGO.ORG',
]


def add_users(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    User = apps.get_model('auth', 'User')

    # Get group
    pg = Group.objects.get(name=GROUP_NAME)

    # Users might not exist yet since they are populated from the LDAP
    for username in USERS:
        user, _ = User.objects.get_or_create(username=username)

        # Add user to group
        pg.user_set.add(user)


def remove_users(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    User = apps.get_model('auth', 'User')

    # Get group
    pg = Group.objects.get(name=GROUP_NAME)

    # Get users
    users = User.objects.filter(username__in=USERS)

    # Remove users
    pg.user_set.remove(*users)


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0021_create_raven_users_group'),
    ]

    operations = [
        migrations.RunPython(add_users, remove_users),
    ]
