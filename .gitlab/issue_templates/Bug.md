## Description of problem
<!--
Describe in detail what you are trying to do and what the result is.
Exact timestamps, error tracebacks, and screenshots (if applicable) are very helpful.
-->


## Expected behavior
<!-- What do you expect to happen instead? -->


## Steps to reproduce
<!-- Step-by-step procedure for reproducing the issue -->


## Context/environment
<!--
Describe the environment you are working in:
    * If using the ligo-gracedb client package, which version?
    * Your operating system
    * Your browser (web interface issues only)
    * If you are experiencing this problem while working on a LIGO or Virgo computing cluster, which cluster are you using?
-->


## Suggested solutions
<!-- Any ideas for how to resolve this problem? -->
